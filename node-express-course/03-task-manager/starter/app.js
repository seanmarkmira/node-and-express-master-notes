console.log('Task Manager Application')

const express = require('express')
const app = express();
const port = 3500;
const tasks = require('./routes/tasks.js')
const connectDB = require('./db/connect')
const notFound = require('./middleware/not-found')
const customErrorHandling = require('./middleware/error-handler')
require('dotenv').config()

app.use(express.static('./public'))
app.use(express.json())
app.use('/api/v1/tasks', tasks)
app.use(notFound)
app.use(customErrorHandling)

const start = async () =>{
	try{
		await connectDB(process.env.MONGO_URI)
		app.listen(port, (req,res)=>{
			console.log('listening to port 3500')
		})
	}catch(error){
		console.log(error)
		console.log('DB failed to connect.')
	}
}

start()