const mongoose = require('mongoose')

//Only the schema made will be accepted by the database
const TaskSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, "This is required"],
		trim: true,
		maxlength:20
	},
	completed:{
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model('Task', TaskSchema)