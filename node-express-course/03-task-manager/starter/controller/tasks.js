//res responses samples:
// res.status(200).json({tasks})
// res.status(200).json({tasks, amount tasks.length})
// res.status.(500).json({msg:error})
// res.status(200).json({...someObject, data:{tasks}})

//The note for this is that if you have one way of responsing in a route, use it in all of your routes to make it easier for other people to utilize your responses

const Task = require('../models/Task')
const asyncWrapper = require('../middleware/async')

const getAllTasks = asyncWrapper(async (req,res) =>{
	const result = await Task.find({})
	res.status(200).json(result)
		// res.status(500).json({msg:"Unssucessful getting results", success:false})
})

const createTask = async (req,res) =>{
	try{
		const task = await Task.create(req.body)
		res.status(200).json({task})
	}catch(error){
		res.status(500).json({msg:"Unssucessful as there is validation error", success:false})
	}
}

const deleteAllTask = async (req,res) =>{
	try{
		const deleteAllTasks = await Task.deleteMany()
		res.status(200).json({msg:"deleted all tasks"})
	}catch(error){
		console.log(error)
		res.status(500).json({msg:"Unssucessful deletion of all tasks", success:false})
	}
	
}

const deleteTask = async (req,res) =>{
	try{
		deleteOneTask = await Task.findOneAndDelete({name:req.params.id})
		if(!deleteOneTask){
			console.log('1')
			res.status(404).json({msg:`there is no document`, success:false})
		}else{
			console.log(deleteOneTask)
			console.log('2')
			res.status(200).json({msg:`deleted the ${req.params.id} document`})
		}
		
	}catch(error){
		res.status(500).json({msg:"Unssucessful deletion of all tasks", success:false})
	}
}

const getTask = async (req,res) =>{

	const getSpecTask = await Task.findOne({name:req.params.id})
	if(!getSpecTask){
		const error = new Error('Not Found')
		error.status = 404
		return next(error)
			//res.status(404).json({msg:`There is no document with ${req.params.id}`, success:false})
	}else{
		res.status(200).json(getSpecTask)
	}
		// const error2 = new Error('Not Found')
		// error2.status = 404
		// return next(error2)
		// res.status(500).json({msg:"Unssucessful getting one task", success:false})
}

const updateTask = async (req,res) => {
	
	const updateTask = await Task.findOneAndUpdate({name:req.params.id}, req.body,{new:true, runValidators:true})

	try{
		if(!updateTask){
			console.log('There is no task found to be updated')
			res.status(404).json({msg:"There is no document"})
		}else{
			console.log('there is a document found')
			res.status(200).json({updateTask})
		}
	}catch(error){
		console.log('erorr in server')
		res.status(500).json({msg:"Server error", success:false})
	}
}

//putTaskSample is just here to demonstrate that essentially it is the same content with updateTask with the extra variable of 'overwrite:true' since we are trying to overwrite the document
//putTaskSample is not being used in this project
const putTaskSample = async (req,res) => {
	const updateTask = await Task.findOneAndUpdate({name:req.params.id}, req.body,{new:true, runValidators:true, overwrite:true})

	try{
		if(!updateTask){
			console.log('There is no task found to be updated')
			res.status(404).json({msg:"There is no document"})
		}else{
			console.log('there is a document found')
			res.status(200).json({updateTask})
		}
	}catch(error){
		console.log('erorr in server')
		res.status(500).json({msg:"Server error", success:false})
	}
}

module.exports = {
	getAllTasks,
	createTask,
	deleteAllTask,
	updateTask,
	deleteTask,
	getTask
}