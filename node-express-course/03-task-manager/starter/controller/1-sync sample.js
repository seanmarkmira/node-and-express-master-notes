//This code is an example of async approach, to where some of the block is not put into one thread and passed to another to make sure that all other code blocks can run without waiting for the result of another

// console.log('Sync 1')
// 
// setTimeout(_=> console.log('timeout2',0))
// 
// Promise.resolve().then(_=>console.log('promise 3'))
// 
// console.log('Sync 4')

//What makes a function async? This is by treating a function as a promise. For example, code below getFruit is already a promise by stating async to the variables

const getFruit = async() => {
	const fruits ={
		fruit1: 'pineapple',
		fruit2: 'peach',
		fruit3: 'strawberry'
	}
	//If you do not use the async() keyword in the function, you can just make use of Promise.resolve as stated below
	// return Promise.resolve(fruits[name])
	return fruits
}

getFruit('peach').then(console.log)
//Rather than doing a chain of .then to promises of getFruit, we can use await. Await means that pause the execution of this block of code until we are finished on the execution of getFruit
//Await on the code below is not representative of real world application, when you daisy chain assignment of value using await, what you can do is use this await on 2 variables if the 2nd variable is dependent on the result of the first assignment
const makeSmoothie = async() =>{
	try{
		const a = await getFruit('pineapple')
		const b = await getFruit('peach')
		const smoothie = Promise.all([a,b])
	}catch(err){
		return smoothie
	}
}