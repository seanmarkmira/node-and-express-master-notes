const mongoose = require('mongoose')

const connectDB = (url) =>{
	return mongoose.connect(url,{
		useUnifiedTopology: true,
		useNewUrlParser: true
	})
}

module.exports = connectDB

// const Cat = mongoose.model('Cat', { name: String });
// 
// const kitty = new Cat({ name: 'Zildjian' });
// kitty.save().then(() => console.log('meow'));