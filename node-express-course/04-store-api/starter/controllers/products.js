//Last tutorial:

//Major learnings: 
/*
1. Sorting through model.find result through resultFindMethod.sort()
2. Utilizing $regex: name $options: i option as a query to our find method
3. Showing specific property only through resultFindMethod.select()
4. There are a alot of functions we can use in the result of: const products = await Product.find({}).select('name price')
5. Limit the result of our model.find result through resultFindMethod.limit()
6. Skip a number of documents in our model.find result through resultFindMethod.skip()
As you can see, we can use products.select('pass an arguement') or products.sort('pass an arguement') to utilize this
7. The skip and limit function of the model.find result is usually used in the paganation of a website. 
8. The use of page logic that can be used in any projects: const skip = (page-1) * limit
9. Catching input of users and changing that input to be able to understand in the language of mongoose (regex and .replace method)
10. Difference between dot notation vs bracket on assigning values: queryObject.field vs queryObject[field]
*/
const Product = require('../models/product')

const getAllProductsStatic = async (req,res) =>{
	const products = await Product.find({price:{$gt:30}})
	.sort('price')
	.select('name price')
	console.log(products)
	//throw new Error('This is just sending an error')
	res.status(200).json({products, nbHits:products.length})
}


const getAllProducts = async (req,res) =>{
	const {featured, company, name, sort, fields, numericFilters} = req.query
	const queryObject = {}

	//We have to replace the numericFilters that is coming inside from > to $gt



	if(featured){
		queryObject.featured = featured === 'true'? true:false
	}
	if(company){
		queryObject.company = company
		
	}
	if(name){
		queryObject.name = {$regex:name, $options:'i'}
	}

	if(numericFilters){
		const operatorMap = {
			'>':'$gt',
			'>=':'$gte',
			'=':'$eq',
			'<':'$lt',
			'<=':'$lte',
			'mira':'the best'
		}
		//regular expression means, if you see any of this elements in a body of string
		const regEx = /\b(<|>|>=|=|<|<=|mira)\b/g
		//What's happening here is that, we use replace method and regular expression to replace a certain input. The arguement regEx in the method replace means: "WHEN YOU SEE THIS STRING, IN THIS USE-CASE, WHEN YOU SEE ANY OF THE STRING IN THE REGULAR EXPRESSION, PUT IT IN A MATCH VARIABLE AND ASSIGN IT TO THE OBJECT operaterMap so that we could change the input for mongoose to understand"
		let filters = numericFilters.replace(regEx,(match)=>`-${operatorMap[match]}-`)
		console.log(filters)
		
		const options = ['price', 'rating']
		//Again, the filters result changes the input of the user to mongoose language. We then split it by , in order to get a seperation if there is more than one filter
		console.log(filters)
		filters = filters.split(',').forEach((item)=>{
			const[field,operator,value] = item.split('-')
			//the options are price and rating and if it exist then add query object with the field price/rating with the operator and number value
			if(options.includes(field)){
				queryObject[field] = {[operator]: Number(value)}
			}
		})
	}

	
	console.log('queryObject')
	console.log(queryObject)
	
	let result = Product.find(queryObject)

	if(sort){
		console.log('This is the result')
		sortList = sort.split(',').join(' ')
		result = result.sort(sortList)
	}else{
		result = result.sort('createdAt')
	}

	if(fields){
		const fieldList = fields.split(',').join(' ')
		result = result.select(fieldList)
	}

	const page = Number(req.query.page) || 1
	const limit = Number(req.query.limit) || 10
	const skip = (page-1) * limit;

	result = result.skip(skip).limit(limit)
	// 23 products / 7 limit = page 1:7, 7 ,7, 2

	const products = await result
	
	res.status(200).json({products, nbHits:products.length})
}

module.exports ={
	getAllProductsStatic,
	getAllProducts
}


