require('dotenv').config()
require('express-async-errors')
//async errors

const express = require('express');
const app = express();
const notFound = require('./middleware/not-found')
const errorMiddleware = require('./middleware/error-handler')
const port = process.env.PORT || 3000
const connectDB = require('./db/connect')
const productsRouter = require('./routes/products')

app.use(express.json())
app.get('/',(req,res)=>{
	res.send('<h1>store API</h1><a href="/api/v1/products">Products Route</a>')
})

app.use('/api/v1/products', productsRouter)

//products route
app.use(notFound)
app.use(errorMiddleware)

const start = async()=>{
	try{
		await connectDB(process.env.MONGO_URI)
		app.listen(port, console.log(`listening to port: ${port}`))
	}catch(error){
		console.log(error)
	}
}

start()