const {Unauthenticated} = require('../errors')
const jwt = require('jsonwebtoken')

const authenticationMiddleware = async (req,res,next) =>{
	const authHeader = req.headers.authorization
	
	if(!authHeader || !authHeader.startsWith('Bearer ')){
		throw new Unauthenticated('No token provided')
	}
	const token = authHeader.split(' ')[1]

	//If token is already available, we can then verify the token utilizing jwt.verify(token, secret)

	try{
		const decoded = jwt.verify(token,process.env.JWT_SECRET)
		console.log('This is inside the decoded')
		console.log(decoded)
		const {id, username} = decoded
		req.user = {id,username}
		next()
	}catch(error){
		throw new Unauthenticated('No token provided')
	}
}

module.exports = authenticationMiddleware
