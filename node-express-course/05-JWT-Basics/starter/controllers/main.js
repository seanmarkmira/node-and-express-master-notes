// check username, password in post(login) request
// if exist create new JWT
// send back to front-end
//setup authentication so only the request with JWT can access the dashboard
const CustomAPIError = require('../errors/custom-error')
const jwt = require('jsonwebtoken')
const {BadRequestError} = require('../errors')

const login = async(req,res) =>{
	const {username, password} = req.body
	console.log('i am here')
	/*
		There are multiple ways to check a blank username or password problem"
			1. mongoose validation, that is through the schema creation
			2. Joi
			3. Check in the controller
	*/


	if(!username || !password){
		throw new BadRequestError('Please provide email and password')
	}
	//The id part is just a demo since we do not have a connection to the DB in this project
	const id = new Date().getDate()
	/*
	Tips on creating a tokenIn creating a token, do not pass a password here as this is a bad practice.
		1. Do not pass the password of the user. This is bad practice
		2. Make the payload small to have better experience to slow connection of the user
		3. When it comes to the JWT secret, use complex and really long strings to make it more secured
	*/
	
	//jwt.sign takes in 3 arguements: payload, your secret, and options 
	const token = jwt.sign({id,username},process.env.JWT_SECRET,{expiresIn:'30d'})
	res.status(200).json({msg:"username and password login endpoint",token:token})
}

const dashboard = async(req,res) =>{
	console.log(req.user)
	const luckyNumber = Math.floor(Math.random()*100)
	res.status(200).json({msg:`Hello, ${req.user.username}`, secret:`Here is your authorized data, your lucky number is ${luckyNumber}`})
	
}

module.exports = {
	login,
	dashboard
}