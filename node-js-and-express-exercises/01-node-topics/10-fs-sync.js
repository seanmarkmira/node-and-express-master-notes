const {readFileSync, writeFileSync} = require('fs');
//The top is all about destructuring which we will only required the specifics methods of the object. It is the same below:
// const fs = require('fs')

const first = readFileSync('../content/subfolder/first.txt', 'utf8')
const second = readFileSync('../content/subfolder/second.txt', 'utf8')

console.log(first, second)

writeFileSync('../content/testpath.txt', `Here is the results:${first}, ${second}`, {flag:'a'})
//The flag a signifies to node that we will append the result to the file