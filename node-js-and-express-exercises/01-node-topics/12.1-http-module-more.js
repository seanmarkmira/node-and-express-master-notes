const http = require('http')
const {readFileSync} = require('fs')

//get all files and getting the content
const homePage = readFileSync('./navbar-app/index.html')
const homeStyles = readFileSync('./navbar-app/styles.css')
const homeImages = readFileSync('./navbar-app/logo.svg')
const homeLogic = readFileSync('./navbar-app/browser-app.js')

const server = http.createServer((req, res)=>{

	//(status code, {content type})
	//if you use text/plain as content type, the status code 200 content type will change to raw text. That's why we need to use text/html for the head to change this
	const url = req.url;
	console.log(url)

	if(url === '/'){
		res.writeHead(200,{'content-type':'text/html'})
		res.write(homePage)
		res.end()
	}else if(url==='/styles.css'){
		res.writeHead(200,{'content-type':'text/css'})
		res.write(homeStyles)
		res.end() 
	}else if(url==='/logo.svg'){
		res.writeHead(200,{'content-type':'image/svg+xml'})
		res.write(homeImages)
		res.end() 
	}else if(url==='/browser-app.js'){
		res.writeHead(200,{'content-type':'text/html'})
		res.write(homeLogic)
		res.end() 
	}else if(url==='/about'){
		res.writeHead(200,{'content-type':'text/html'})
		res.write(`<h1>About Page</h1>`)
		res.end() 
	}else if(url==='/about'){
		res.writeHead(200,{'content-type':'text/html'})
		res.write(`<h1>About Page</h1>`)
		res.end() 
	}else{
		res.writeHead(404,{'content-type':'text/html'})
		res.write(`<h1>Warning no pages found</h1>`)
		res.end()
	}
	console.log(req.method)
	console.log(req.url)
	
	// res.end(`<h1>Home Page</h1>`)

})

server.listen(4000)