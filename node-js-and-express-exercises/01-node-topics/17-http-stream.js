var http = require('http')
var fs = require('fs')

http.createServer(function(req,res){
	//const text = fs.readFileSync('../content/subfolder/big.txt','utf-8')
	const fileStream = fs.createReadStream('../content/subfolder/big.txt','utf-8')
	fileStream.on('open', ()=>{
		fileStream.pipe(res)
	})
	fileStream.on('error', (err)=>{
		res.end(err)
	})
	//res.end(text)
})
.listen(4000)

