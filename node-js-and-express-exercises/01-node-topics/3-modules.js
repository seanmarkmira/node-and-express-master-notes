// const amount = 12;

// if(amount<10){
// 	console.log("small number");
// }else{
// 	console.log("large number");
// }

// console.log(`hey it's my first node app!!!`);

//Topic Modules
// CommonJS (Back of the hood of JS), every file is module (by default)
// Modules - Encapsulated code (only share minimum)
//This module is connected with names and utils
const sayHi = require('./5-utils')
const names = require('./4-names')
const data = require('./6-alternative-flavor')
console.log(names)
require('./7-mind-grenade')
 
// sayHi('susan')
// sayHi(names.john)
// sayHi(names.peter)

console.log(data)
