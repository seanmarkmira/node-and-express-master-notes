const express = require('express');
const app = express();
const path = require('path');

// setup static and middleware
app.use(express.static('./public'))

//do note that the code block below is commented out as index.html is always treated as root by express
// app.get('/',((req,res)=>{
// 	res.sendFile(path.resolve(__dirname,'./navbar-app/index.html'))
// }))

app.all('*',(req,res)=>{
	res.status(404).send('resource not found');
})

app.listen(4000, ()=>{
	console.log('Server is lisntening in port 4000');
})