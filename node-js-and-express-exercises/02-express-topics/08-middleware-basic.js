const express = require('express')
const app = express()

// req => middleware => res

//If we are going to utilize a middleware, we must either pass it to another middle ware or method or end it with res.send('something')(terminate) 
const logger = (req,res,next) => {
	const method = req.method;
	const url = req.url;
	const time = new Date().getFullYear();
	console.log(method, url, time)
	// res.send('Testing')
	next()
}

app.get('/', logger, (req,res)=>{
	res.send('Home')
})

app.get('/about', (req,res)=>{
	res.send('About')
})

app.listen(4000,()=>{
	console.log('Server is listening n port 4000.')
})

//logger will be executed first before any app.get. This is how middleware works. App.get is still considered a middleware. The idea of an middleware is to create a logic prior to presenting a response. For example, if you want to check if the user is authenticated or is an admin, then present this specific response. 
//Remember that we use next() to pass it it to the app.get but if app.get has a next() it throws back the logic back to the middleware who initiated the next. Without a proper exit in a middleware, the code will not exit properly.