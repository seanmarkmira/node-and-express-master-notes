const express = require('express')
const app = express()
const people = require("./routes/people")
const login = require("./routes/auth")
// static assets
app.use(express.static('./methods-public'))

//Traditional way = more on utilization of HTML and form to get the data. See index.html in the methods-public folder
//We use the express.urlencoded so that in the http post methods, we can utilize the body method of the req. This middleware helps us parse the incoming data and use the req.body
app.use(express.urlencoded({extended:false}))

//parse json
app.use(express.json())
app.use('/api/people', people)
app.use('/login', login)


app.listen(3000, ()=>{
	console.log('Server is listening at 3000')
})