const express = require('express');
const app = express()
//data module.exported from data.js, please look for the directory in the cloned repo
const data = require('./data.js')

app.get('/',(req,res)=>{
	res.json(data.products)
	// res.json(data.people)
})

app.listen(4000, ()=>{
	console.log('Server is listening to port 4000');
})