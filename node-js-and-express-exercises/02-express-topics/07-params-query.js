const express = require('express');
const app = express()
const {products} = require('./data.js')


app.get('/',(req,res)=>{
	// res.json(data.products)
	// res.json(data.people)
	res.send(`<h1>Home Page</h1> <a href="/api/products">products</a><a href="/api/products/:productID/reviews/:reviewID"><br>reviews</a>`)
})

//With the link above in the res.send, as users click the products link, it will redirect them to /api/products
//The code block belows shows the different data results and just including specific information from the JSON using map
// app.get('/api/products',(req,res)=>{
// 	const newProducts = products.map((product)=>{
// 		const {id, name, image} = product;
// 		return {id, name, image}
// 	})
// 	res.json(newProducts)
// })

//This is one way of showing json, but is not effecient since 1 product only
// app.get('/api/products/1',(req,res)=>{
// 	const singleProduct = products.find((product)=> product.id=== 1)
// 	res.json(singleProduct)
// })

app.get('/api/products/:productID',(req,res)=>{
	// console.log(req)
	console.log('This is req.params:')
	console.log(req.params)
	console.log('This is req.params.productID:')
	console.log(req.params.productID)

	const {productID} = req.params;
	const singleProduct = products.find((product)=> product.id === Number(productID))
	
	if(!singleProduct){
		return res.status(404).send('product does not exist')
	}
	
	return res.json(singleProduct)
})

//we are utilizing a route which is userDefined as used below ':productID'
app.get('/api/products/:productID/reviews/:reviewID',(req,res)=>{
	res.send('hello world')
})

//we now are using query in this section. As tested, you can only use query
//VERY IMPORTANT, in every request, there is one response. IF YOU DO NOT PUT ANY RETURN, JS will take this as the request has not yet been fulfilled. It should RETURN something
app.get('/api/v1/query',(req,res)=>{
	console.log(req.query)
	const {search, limit} = req.query
	let sortedProducts = [...products];

	if(search){
		sortedProducts = sortedProducts.filter((product)=>{
			return product.name.startsWith(search)
		})
	}

	if(limit){
		sortedProducts = sortedProducts.slice(0,Number(limit))
	}

	if(sortedProducts.length<1){
		//res.status(200).send('Nothing cameback on the query')
		return res.status(200).json({success:true, data:[]})
	}

	res.status(200).json(sortedProducts)
	// res.send('hello world')
})

app.listen(4000, ()=>{
	console.log('Server is listening to port 4000');
})