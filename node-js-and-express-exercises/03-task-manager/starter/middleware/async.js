const asyncWrapper = (functionPassed) => {
	return async(req,res,next) =>{
		try{
			await functionPassed(req,res,next)
		}catch(error){
			next(error)
		}
	}
}

module.exports = asyncWrapper