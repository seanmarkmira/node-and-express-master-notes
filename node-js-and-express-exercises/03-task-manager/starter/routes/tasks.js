const express = require('express')
const router = express.Router()
const {getTask, getAllTasks, createTask, deleteAllTask, updateTask, deleteTask} = require('../controller/tasks')

router.route('/').get(getAllTasks).post(createTask).delete(deleteAllTask)
router.route('/:id').get(getTask).patch(updateTask).delete(deleteTask)
module.exports = router

//v1 is given as a design approach to where we can add a v2 in the future if needed
//app.get('/api/v1/tasks')
//app.post('/api/v1/tasks')
//app.get('/api/v1/tasks/:id')
//app.patch('/api/v1/tasks/:id')
//app.delete('/api/v1/tasks')