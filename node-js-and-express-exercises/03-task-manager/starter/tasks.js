const express = require('express')
const router = express.Router()
const app = express()
const taskController = require('../controllers/tasks.js')

router.route('/').get(getAllTask)

module.exports = router